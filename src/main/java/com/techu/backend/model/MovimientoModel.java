package com.techu.backend.model;

import java.time.LocalDate;

public class MovimientoModel {
    private String id_meta;
    private String cuenta_origen;
    private String cuenta_destino;
    private LocalDate fecha_valor;
    private String divisa;
    private Double importe;

    public MovimientoModel( String cuenta_origen, String cuenta_destino, LocalDate fecha_valor, String divisa, Double importe) {
       // this.id_meta = id_meta;
        this.cuenta_origen = cuenta_origen;
        this.cuenta_destino = cuenta_destino;
        this.fecha_valor = fecha_valor;
        this.divisa = divisa;
        this.importe = importe;
    }

    public String getId_meta() {
        return id_meta;
    }

    public void setId_meta(String id_meta) {
        this.id_meta = id_meta;
    }

    public String getCuenta_origen() {
        return cuenta_origen;
    }

    public void setCuenta_origen(String cuenta_origen) {
        this.cuenta_origen = cuenta_origen;
    }

    public String getCuenta_destino() {
        return cuenta_destino;
    }

    public void setCuenta_destino(String cuenta_destino) {
        this.cuenta_destino = cuenta_destino;
    }

    public LocalDate getFecha_valor() {
        return fecha_valor;
    }

    public void setFecha_valor(LocalDate fecha_valor) {
        this.fecha_valor = fecha_valor;
    }

    public String getDivisa() {
        return divisa;
    }

    public void setDivisa(String divisa) {
        this.divisa = divisa;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }
}
