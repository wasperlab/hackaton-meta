package com.techu.backend.model;

import java.time.LocalDate;
import java.util.List;

public class MetaModel {

    private String nombre;
    private LocalDate fecha_alta;
    private LocalDate fecha_fin;
    private Double importe_objetivo;
    private Double importe_conseguido;
    private FotoModel photo;


//private String cuenta_meta
    //private String Status;



    private List<MovimientoModel> movimientoModelList;

    public MetaModel(String nombre, LocalDate fecha_fin, Double importe_objetivo,FotoModel photo) {

        this.nombre = nombre;
        this.fecha_alta=LocalDate.now();
        this.fecha_fin = fecha_fin;
        this.importe_objetivo = importe_objetivo;
        this.importe_conseguido=0.0;
        this.photo=photo;
    }
    public FotoModel getFoto() {
        return photo;
    }

    public void setFoto(FotoModel photo) {
        this.photo = photo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public LocalDate getFecha_alta() {
        return fecha_alta;
    }

    public void setFecha_alta(LocalDate fecha_alta) {
        this.fecha_alta = fecha_alta;
    }

    public LocalDate getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(LocalDate fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public Double getImporte_objetivo() {
        return importe_objetivo;
    }

    public void setImporte_objetivo(Double importe_objetivo) {
        this.importe_objetivo = importe_objetivo;
    }

    public Double getImporte_conseguido() {
        return importe_conseguido;
    }

    public void setImporte_conseguido(Double importe_conseguido) {
        this.importe_conseguido = importe_conseguido;
    }

    /*
    public List<MovimientoModel> getMovimientoModelList() {
        return movimientoModelList;
    }

    public void setMovimientoModelList(List<MovimientoModel> movimientoModelList) {
        this.movimientoModelList = movimientoModelList;
    }

     */
}

