package com.techu.backend.controller;

import com.techu.backend.model.MetaModel;
import com.techu.backend.service.MetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${url.base}")
@CrossOrigin(origins="*", methods={RequestMethod.GET, RequestMethod.POST})
public class MetaController {

    @Autowired
    private MetaService metaService;
    //GET consulta metas creadas
    @GetMapping("/meta")
    public List<MetaModel> getMetas(){

        return metaService.getMetaList();
    }
    //POST crear meta
    @PostMapping("/meta")
    public ResponseEntity postmetas(@RequestBody MetaModel newMeta){
        metaService.addMeta(newMeta);
        return new ResponseEntity<>("meta creado correctamente", HttpStatus.CREATED);
    }

    //GET a un único meta
    @GetMapping("/meta/{nombre}")
    public ResponseEntity getmetasById(@PathVariable String nombre){
        MetaModel pr=metaService.getMetaById(nombre);
        if (pr ==null){
            //no existe el meta
            return new ResponseEntity<>("No hay metas registradas", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    //DELETE borrar meta
    @DeleteMapping ("/meta/{nombre}")
    public ResponseEntity deleteProductById(@PathVariable String nombre){
        if(!metaService.removeMetaById(nombre)){
            //no existe el meta
            return new ResponseEntity<>("meta no encontrado", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity("meta borrado correctamente",HttpStatus.OK);
    }

    /*
    //GET a un único meta
    @GetMapping("/meta/{nombre}")
    public ResponseEntity getmetasById(@PathVariable String nombre){
        MetaModel pr=metaService.getMetaById(nombre);
        if (pr ==null){
            //no existe el meta
            return new ResponseEntity<>("No hay metas registradas", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }



    //PATCH actualizar meta
    @PatchMapping ("/meta/{id}/{importe}")
    public ResponseEntity putmetas(@PathVariable String id,@PathVariable Double importe){
        MetaModel pr= metaService.updateMetaImporteObjetivoById(id,importe);
        if (pr ==null){
            //no existe el meta
            return new ResponseEntity<>("meta no encontrada", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("meta actualizada correctamente",HttpStatus.OK);

    }

    //PACTH actualizar meta basics
    @PatchMapping ("/meta/{id}")
    public ResponseEntity patchmetas(@PathVariable String id,@RequestBody MetaBasicModel metaBasicModel){
        MetaModel pr= metaService.updateMetaBasicById(id,metaBasicModel);
        if (pr ==null){
            //no existe el meta
            return new ResponseEntity<>("meta no encontrada", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("meta actualizada correctamente",HttpStatus.OK);

    }

    //DELETE borrar meta
    @DeleteMapping ("/meta/{id}")
    public ResponseEntity deleteProductById(@PathVariable String id){
        if(metaService.removeMetaById(id) ==-1){
            //no existe el meta
            return new ResponseEntity<>("meta no encontrado", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity("meta borrado correctamente",HttpStatus.OK);
    }

    //GET todos los movimientos
    @GetMapping("/meta/{id}/movimiento")
    public ResponseEntity getUsers(@PathVariable String id){
        MetaModel pr=metaService.getMetaById(id);
        if (pr ==null){
            //no existe el meta
            return new ResponseEntity<>("meta no encontrado", HttpStatus.NOT_FOUND);
        }
        if (pr.getMovimientoModelList()!=null){

            return ResponseEntity.ok(pr.getMovimientoModelList());
        }else{
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    //POST añadir movimimientos
    @PostMapping ("/meta/{id}/movimiento")
    public ResponseEntity addMetaMovimientoById (@PathVariable String id,@RequestBody MovimientoModel movimientoModel){
        MetaModel pr=metaService.getMetaById(id);
        if (pr ==null){
            //no existe el meta
            return new ResponseEntity<>("meta no encontrado", HttpStatus.NOT_FOUND);
        }
        try {
            metaService.addMovimientoMeta(id,movimientoModel);
            return new ResponseEntity<>("Movimiento añadido correctamente",HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity<>(ex,HttpStatus.UNPROCESSABLE_ENTITY);

        }

    }

    //GET Aportaciones de una meta
    @GetMapping("/meta/{id}/aportacion")
    public ResponseEntity getMetaAportacionById(@PathVariable String id){
        MetaModel meta=metaService.getMetaById(id);
        if (meta ==null){
            //no existe el meta
            return new ResponseEntity<>("meta no encontrado", HttpStatus.NOT_FOUND);
        }

        if (meta.getAportacion_periodica()!=null){
            return ResponseEntity.ok(meta.getAportacion_periodica());
        }else{
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

    //PUT Añadir/modificar una aportacion
    @PutMapping ("/meta/{id}/aportacion")
    public ResponseEntity patchMetaAportacionById (@PathVariable String id,@RequestBody AportacionModel aportacionModel){
        MetaModel pr=metaService.getMetaById(id);
        if (pr ==null){
            //no existe el meta
            return new ResponseEntity<>("meta no encontrado", HttpStatus.NOT_FOUND);
        }
        metaService.updateMetaAportacion(id,aportacionModel);
        return new ResponseEntity<>("Aportacion añadida correctamente",HttpStatus.OK);

    }

     */
}
