package com.techu.backend.service;

import com.techu.backend.model.*;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;


@Component
public class MetaService {

    private ArrayList<MetaModel> metaList = new ArrayList<>();

    public MetaService() {

        FotoModel fotoModel=new FotoModel("/img/usuario.jpg","meta");
        metaList.add(new MetaModel("coche de mis sueños", LocalDate.parse("2020-12-31"), 100.50,fotoModel));
        metaList.add(new MetaModel("Regalos de navidad", LocalDate.parse("2020-12-31"), 500.50,fotoModel));
        /*ArrayList<MovimientoModel> movimientos = new ArrayList<>();
        movimientos.add(new MovimientoModel("ESOOOOOOOOO","ESDDDDDDD",LocalDate.parse("2020-03-01"),"EUR",333.33));
        movimientos.add(new MovimientoModel("ESOOOOOOOOO","ESDDDDDDD",LocalDate.parse("2020-03-01"),"EUR",222.00));
        movimientos.add(new MovimientoModel("ESOOOOOOOOO","ESDDDDDDD",LocalDate.parse("2020-05-01"),"EUR",111.33));
        metaList.get(1).setMovimientoModelList(movimientos);

         */
    }

    // READ productos
    public ArrayList<MetaModel> getMetaList() {
        return metaList;
    }

    // READ instance (por ID)
    public MetaModel getMetaById(String id) {
        int index=getIndex(id);
        if (index>=0){
            return metaList.get(index);
        }
        return null;
    }

    public MetaModel addMeta (MetaModel metaModel){
        metaList.add(metaModel);
        return metaModel;
    }

    public boolean removeMetaById(String nombre){
        int pos = getIndex(nombre);
        if (pos >= 0) {
            metaList.remove(pos);
            return true;
        }
        return false;
    }

    // Devuelve la posición de un producto en productoList
    public int getIndex(String nombre) {
        int i = 0;
        while (i < metaList.size()) {
            if (metaList.get(i).getNombre().equals(nombre)) {
                return(i); }
            i++; }
        return -1;
    }


/*
    // GET mayores que parámetro 'max'
    public ArrayList<ProductoModel> getMayores(int max){
        ArrayList<ProductoModel> mayores = new ArrayList<>();
        for (int i=0; i < productoList.size(); i++) {
            if (productoList.get(i).getPrecio() > max) {
                mayores.add(productoList.get(i));
            }
        }
        return mayores;
    }

    // POST movimiento
    public MetaModel addMovimiento(String id,MovimientoModel nuevomov) {
        int index=getIndex(id);
        if (index>=0){
            metaList.get(index).setImporte_conseguido(metaList.get(index).getImporte_conseguido()+ nuevomov.getImporte());
            metaList.get(index).getMovimientoModelList().add(nuevomov);
            return metaList.get(index);
        }

        return null;
    }

    // UPDATE
    public ProductoModel updateMetaId(String id, MetaModel newPro){
        int pos = getIndex(id);
        if (pos >= 0) {
            productoList.set(pos, newPro);
            return productoList.get(pos);
        }
        return null;
    }

    // Partial UPDATE
    public ProductoModel updateProductoPrecioById(String id, double newPrecio){
        int pos = getIndex(id);
        if (pos >= 0) {
            ProductoModel pr = productoList.get(pos);
            pr.setPrecio(newPrecio);
            productoList.set(pos, pr);
            return productoList.get(pos);
        }
        return null;
    }

    // DELETE
    public void removeProductoById(String id) {
        int pos = getIndex(id);
        if (pos >= 0) {
            productoList.remove(pos);
        }
    }
 */



}